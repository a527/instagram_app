お世話になっております。今週課題に取り掛かる時間が見込めないため、２週間経っていませんが提出させていただきます。
【ログインアカウント】
email: example@railstutorial.org
password: foobar

【heroku】
https://fake-insta-gram.herokuapp.com/


<未実装>

・Facebookログイン

…deviseのドキュメントなどを読みながら行ったのですが、Facebookとの連携がうまくいきませんでした。

・テスト(一部分)

…rspecを初めて扱い、苦労しました。Seleniumの設定に手こずり、System Specのテストの記述がどうしてもできませんでした。
本課題では記述できるように、調べていきたいと思います。


その他、gemを魔法のように利用している部分も多かったので、ドキュメントを読み、仕様を理解しながら使うことを心がけたいです。




