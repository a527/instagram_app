# frozen_string_literal: true

class CommentsController < ApplicationController
  before_action :authenticate_user!, only: %i[create destroy]
  before_action :correct_user, only: [:destroy]

  # POST /comments
  def create
    @comment = current_user.comments.build(comment_params)
    @comment.micropost_id = params[:micropost_id]

    if @comment.save
      post = Micropost.find(@comment.micropost_id)
      post.create_notification_comment!(current_user, @comment.id)
    end
    redirect_to request.referrer || root_url
  end

  # DELETE /comments/:id
  def destroy; end

  private

  def comment_params
    params.require(:comment).permit(:content)
  end

  def correct_user
    @micropost = current_user.microposts.find(params[:id])
    redirect_to root_url if @micropost.nil?
  end
end
