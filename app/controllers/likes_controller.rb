# frozen_string_literal: true

class LikesController < ApplicationController
  # ログインしているか
  before_action :authenticate_user!, only: %i[create destroy]

  # POST /likes
  def create
    @post = Micropost.find(params[:micropost_id])
    current_user.add_like(@post)

    # いいねされた場合に通知を作成する
    @post.create_notification_like!(current_user)

    # Ajax化できていない
    respond_to do |format|
      format.html { redirect_to request.referrer || root_url }
      # format.js
    end
  end

  # DELETE /likes/:id
  def destroy
    @post = Like.find(params[:id]).micropost
    current_user.remove_like(@post)

    # Ajax化できていない
    respond_to do |format|
      format.html { redirect_to request.referrer || root_url }
      # format.js
    end
  end
end
