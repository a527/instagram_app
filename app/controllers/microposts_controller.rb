# frozen_string_literal: true

class MicropostsController < ApplicationController
  before_action :authenticate_user!, only: %i[create destroy]
  before_action :correct_user, only: [:destroy]

  # POST /microposts/:id
  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      # Success
      redirect_to root_path
    else
      # Failure
      @feed_items = []
      render 'static_pages/home'
    end
  end

  # DELETE /microposts/:id
  def destroy
    @micropost.destroy
    redirect_to root_path
  end

  def new
    @micropost = current_user.microposts.build if user_signed_in?
  end

  def index
    @microposts = Micropost.where('content like ?', "%#{params[:search]}%")
    @comment = current_user.comments.build
    @hit_user_names = User.where('user_name like ?', "%#{params[:search]}%")
    @hit_full_names = User.where('full_name like ?', "%#{params[:search]}%")
  end

  private

  def micropost_params
    params.require(:micropost).permit(:content, :picture)
  end

  # 他人のポストに削除アクセスした場合
  def correct_user
    @micropost = current_user.microposts.find_by(id: params[:id])
    redirect_to root_url if @micropost.nil?
  end
end
