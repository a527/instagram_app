# frozen_string_literal: true

class RelationshipsController < ApplicationController
  # ログインしているか
  before_action :authenticate_user!, only: %i[create destroy]

  # POST /relationships
  def create
    @user = User.find(params[:followed_id])
    current_user.follow(@user)

    @user.create_notification_follow!(current_user)

    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

  # DELETE /relationships/:id
  def destroy
    @user = Relationship.find(params[:id]).followed
    current_user.unfollow(@user)

    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end
end
