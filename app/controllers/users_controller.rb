# frozen_string_literal: true

class UsersController < ApplicationController
  # ログインしているか
  before_action :authenticate_user!, only: %i[update edit show index following followers likes]
  # 自分自身かどうか
  before_action :correct_user, only: %i[update edit destroy]

  # GET /users/:id
  def show
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
    # 一覧画面からもコメントを送る
    @comment = current_user.comments.build
  end

  # GET /users/:id/edit
  def edit
    @user = User.find(params[:id])
  end

  # PATCH /users/:id
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params_update)
      redirect_to @user
    else
      # Failure
      # => @user.errors.full_messages()
      render 'edit'
    end
  end

  # DELETE /users/:id
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    redirect_to root_url
  end

  # GET /users
  def index
    @users = User.all.paginate(page: params[:page])
  end

  # フォローしている人一覧
  # GET /users/:id/following
  def following
    @title = 'フォロー'
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  # フォロワー一覧
  # GET /users/:id/follower
  def followers
    @title = 'フォロワー'
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  # いいね一覧
  # GET /users/:id/likes
  def likes
    @title = 'お気に入り'
    @user = User.find(params[:id])
    @posts = @user.likes.paginate(page: params[:page])
    @comment = current_user.comments.build
    render 'show_like'
  end

  private

  def user_params_update
    params.require(:user).permit(:full_name, :user_name, :email, :gender,
                                 :website, :introduction, :profile_picture)
  end

  # 他人の編集ページに直接アクセスした場合
  def correct_user
    @user = User.find(params[:id])
    redirect_to root_url unless current_user == @user
  end
end
