# frozen_string_literal: true

class Micropost < ApplicationRecord
  belongs_to :user
  default_scope -> { order(created_at: :desc) }

  mount_uploader :picture, PictureUploader

  validates :user_id, presence: true
  validates :content, length: { maximum: 140 }
  validates :picture, presence: true

  has_many  :liked_relationships, class_name: 'Like',
                                  foreign_key: :micropost_id,
                                  dependent: :destroy

  # 投稿をいいねしているユーザ一覧
  has_many :liked_by,
           through: :liked_relationships,
           source: :user

  has_many :comments, dependent: :destroy
  has_many :notifications, dependent: :destroy

  # (current_user)によってmicropostがいいねされた時に呼び出され、通知を作成する
  def create_notification_like!(current_user)
    # すでに「いいね」されているか
    tmp = Notification.where(['give_id = ? and given_id = ? and micropost_id = ? and action = ? ', current_user.id, user_id, id, 'like'])
    # いいねされていない場合のみ、通知する
    if tmp.blank?
      # current_user発の通知
      notification = current_user.active_notifications.new(
        micropost_id: id,
        given_id: user_id,
        action: 'like'
      )
      # 自作自演いいねの場合わざわざ通知しない
      if notification.give_id == notification.given_id
        notification.checked = true
      end
      notification.save if notification.valid?
    end
  end

  def create_notification_comment!(current_user, comment_id)
    # コメントは複数回することが考えられるため、１つの投稿に複数回通知する
    notification = current_user.active_notifications.new(
      micropost_id: id,
      comment_id: comment_id,
      given_id: user_id,
      action: 'comment'
    )
    # 自作自演コメントの場合はわざわざ通知しない
    notification.checked = true if notification.give_id == notification.given_id
    notification.save if notification.valid?
  end
end
