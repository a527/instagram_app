# frozen_string_literal: true

class Notification < ApplicationRecord
  default_scope -> { order(created_at: :desc) }
  belongs_to :micropost, optional: true
  belongs_to :comment, optional: true

  belongs_to :give, class_name: 'User', foreign_key: 'give_id', optional: true
  belongs_to :given, class_name: 'User', foreign_key: 'given_id', optional: true
end
