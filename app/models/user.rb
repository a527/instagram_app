# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  mount_uploader :profile_picture, ProfilePictureUploader
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :omniauthable, omniauth_providers: %i[facebook]

  has_many :microposts, dependent: :destroy

  has_many :comments, dependent: :destroy
  # has_many :relationships, dependent: :destroy
  has_many :active_relationships, class_name: 'Relationship',
                                  foreign_key: :follower_id,
                                  dependent: :destroy
  # 僕のフォローしている人たち(さらなる簡略化)
  # @user.active_relationships.map {|r| r.followed }
  # @user.following
  has_many :following,
           through: :active_relationships,
           source: :followed

  has_many :passive_relationships, class_name: 'Relationship',
                                   foreign_key: :followed_id,
                                   dependent: :destroy

  # 僕のフォロワーいちらん
  has_many :followers,
           through: :passive_relationships,
           source: :follower

  has_many :likes_relationships, class_name: 'Like',
                                 foreign_key: :user_id,
                                 dependent: :destroy

  # ユーザーがいいねしている投稿一覧
  has_many :likes,
           through: :likes_relationships,
           source: :micropost

  # 通知
  # 自分(=give_id)が（誰かに）与えた通知
  has_many :active_notifications, class_name: 'Notification', foreign_key: 'give_id', dependent: :destroy
  # 自分宛(=given_id)の通知
  has_many :passive_notifications, class_name: 'Notification', foreign_key: 'given_id', dependent: :destroy

  # 新規作成・更新時
  # コールバック=>
  before_save { self.email = email.downcase }

  # 新規作成時のみ（更新時は反応しない）
  # before_create :create_activation_digest

  # Validation
  validates :full_name, presence: true, length: { maximum: 50 }

  INVALID_NAME_REGEX = /( |　)+/.freeze
  validates :user_name, presence: true, length: { maximum: 30 },
                        uniqueness: true,
                        format: { without: INVALID_NAME_REGEX }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i.freeze
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  # has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

  # フォロー関連
  def follow(user)
    # self.active_relationships.create(followed_id: user.id)
    # self.following << user
    following << user
  end

  def unfollow(user)
    active_relationships.find_by(followed_id: user.id).destroy
  end

  def following?(user)
    following.include?(user)
  end

  def feed
    following_ids = "SELECT followed_id FROM relationships
                     WHERE follower_id = :user_id"
    Micropost.where("user_id IN (#{following_ids})
                     OR user_id = :user_id", user_id: id)
  end

  # お気に入り関連
  def add_like(post)
    likes << post
  end

  def remove_like(post)
    likes_relationships.find_by(micropost_id: post.id).destroy
  end

  def likes?(post)
    likes.including?(post)
  end

  # コメント関連
  def add_comment(comment, post); end

  def remove_comment(comment, post); end

  # 通知関連 current_userがuserをフォローした時userに通知する
  def create_notification_follow!(current_user)
    temp = Notification.where(['give_id = ? and given_id = ? and action = ? ', current_user.id, id, 'follow'])
    if temp.blank?
      notification = current_user.active_notifications.new(
        given_id: id,
        action: 'follow'
      )
      notification.save if notification.valid?
    end
  end
end
