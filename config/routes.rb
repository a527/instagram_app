# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    passwords: 'users/passwords'
  }

  resources :users, only: %i[show edit update index]
  resources :microposts, only: %i[create destroy index]

  resources :users do
    member do
      get :following, :followers, :likes
    end
  end

  resources :microposts do
    member do
      get :liked
    end
  end

  resources :relationships, only: %i[create destroy]
  resources :likes, only: %i[create destroy]
  resources :comments, only: %i[create destroy]
  resources :notifications, only: :index

  root 'static_pages#home' # => root_path
  get  '/help',    to: 'static_pages#help'
  get  '/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'
end
