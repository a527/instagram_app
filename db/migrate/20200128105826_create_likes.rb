# frozen_string_literal: true

class CreateLikes < ActiveRecord::Migration[5.1]
  def change
    create_table :likes do |t|
      t.integer :user_id
      t.integer :micropost_id

      t.timestamps
    end
    # 高速化
    add_index :likes, :user_id
    add_index :likes, :micropost_id
    # 一意性のため（ポスト・ユーザの組）
    add_index :likes, %i[user_id micropost_id], unique: true
  end
end
