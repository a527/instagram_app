# frozen_string_literal: true

class CreateNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications do |t|
      t.integer :give_id, null: false
      t.integer :given_id, null: false
      t.integer :micropost_id
      t.integer :comment_id
      t.string :action, default: '', null: false
      t.boolean :checked, default: false, null: false
      t.timestamps
    end
    add_index :notifications, :give_id
    add_index :notifications, :given_id
    add_index :notifications, :micropost_id
    add_index :notifications, :comment_id
  end
end
