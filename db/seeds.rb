# frozen_string_literal: true

User.create!(full_name: 'Example User',
             user_name: 'exampleuser',
             email: 'example@railstutorial.org',
             password: 'foobar',
             password_confirmation: 'foobar')

50.times do |n|
  full_name = Faker::Pokemon.unique.name

  user_name = full_name.downcase
  user_name.gsub!(' ', '')

  email    = "example-#{n + 1}@railstutorial.org"
  password = 'password'
  User.create!(full_name: full_name,
               user_name: user_name,
               email: email,
               password: password,
               password_confirmation: password)
end

# ユーザーを登録された順に並べて最初の６人だけをとる
users = User.order(:created_at).take(6)
50.times do
  content =
    Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content, picture: File.open(File.join(Rails.root, '/app/assets/images/default.png'))) }
end

# リレーションシップ
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }

# いいね
users = User.all
user  = User.first
posts = Micropost.all
liked_posts = posts[2..20]
liked_posts.each { |post| user.add_like(post) }
