# frozen_string_literal: true

FactoryBot.define do
  factory :examplepost, class: Micropost do
    content { 'Hello' }
    picture { Rack::Test::UploadedFile.new(File.join(Rails.root, 'app/assets/images/default.png')) }
    association :user
  end

  factory :example_post2, class: Micropost do
    content { 'Hello' }
    picture { Rack::Test::UploadedFile.new(File.join(Rails.root, 'app/assets/images/default.png')) }
    association :user
  end

  factory :michael_post, class: Micropost do
    content { 'Hello' }
    picture { Rack::Test::UploadedFile.new(File.join(Rails.root, 'app/assets/images/default.png')) }
    association :user
  end

  factory :hartl_post, class: Micropost do
    content { 'Hello' }
    picture { Rack::Test::UploadedFile.new(File.join(Rails.root, 'app/assets/images/default.png')) }
    association :user
  end
end
