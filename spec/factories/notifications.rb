# frozen_string_literal: true

FactoryBot.define do
  factory :notification do
    give_id { 1 }
    given_id { 1 }
    micropost_id { 1 }
    comment_id { 1 }
    action { 'MyString' }
    checked { false }
  end
end
