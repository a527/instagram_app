# frozen_string_literal: true

FactoryBot.define do
  factory :user_to_michael, class: Relationship do
    association :follower
    association :followed
  end
end
