# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    full_name { 'Example' }
    user_name { 'example' }
    email { 'example@example.com' }
    password { 'password' }
  end

  factory :michael, class: User do
    full_name { 'Michael' }
    user_name { 'michael' }
    email { 'michael@example.com' }
    password { 'password' }
  end

  factory :hartl, class: User do
    full_name { 'Hartl' }
    user_name { 'hartl' }
    email { 'hartl@example.com' }
    password { 'password' }
  end
end
