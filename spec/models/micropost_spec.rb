# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Micropost, type: :model do
  require 'rails_helper'

  # FactoryBot set up
  before do
    @user = FactoryBot.build(:user)
    @most_recent = FactoryBot.build(:most_recent)
  end

  describe 'Validation' do
    test 'should be valid' do
      assert @micropost.valid?
    end

    test 'user id should be present' do
      @micropost.user_id = nil
      assert_not @micropost.valid?
    end

    test 'content should be at most 140 characters' do
      @micropost.content = 'a' * 141
      assert_not @micropost.valid?
    end

    test 'order should be most recent first' do
      assert_equal @most_recent, Micropost.first
    end
  end
end
