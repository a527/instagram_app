# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Relationship, type: :model do
  factory :relationship do
    association :follower
    association :followed
  end
end
