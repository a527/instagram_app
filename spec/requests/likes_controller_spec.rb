# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LikesController, type: :request do
  describe 'create: POST /likes' do
    context 'ログインユーザの場合' do
      before do
        @user = FactoryBot.create(:user)
        @michael = FactoryBot.create(:michael)
        sign_in @user
        @post = FactoryBot.create(:michael_post, user: @michael)
      end

      example '正常なレスポンスを返す' do
        post likes_path, params: { micropost_id: @post.id }
        expect(response.status).to eq 302
      end

      example 'お気に入り件数が増えている' do
        expect do
          post likes_path, params: { micropost_id: @post.id }
        end.to change(@user.likes, :count).by(1)
      end
    end

    context 'ゲストの場合' do
      before do
        @user = FactoryBot.create(:user)
        @michael = FactoryBot.create(:michael)
        @post = FactoryBot.create(:michael_post, user: @michael)
      end

      example 'ログイン画面に遷移' do
        post likes_path, params: { micropost_id: @post.id }
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end
end
