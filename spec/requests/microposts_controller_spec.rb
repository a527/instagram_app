# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MicropostsController, type: :request do
  describe 'create: #POST /microposts' do
    context 'ログインユーザの場合' do
      before do
        @user = FactoryBot.create(:user)
        sign_in @user
      end

      example '正常なレスポンスを返す' do
        micropost_params = FactoryBot.attributes_for(:examplepost)
        post microposts_path, params: { micropost: micropost_params }
        expect(response.status).to eq 302
      end

      example '正常に投稿できる' do
        micropost_params = FactoryBot.attributes_for(:examplepost)
        expect do
          post microposts_path, params: { micropost: micropost_params }
        end.to change(@user.microposts, :count).by(1)
      end

      example 'パラメータ不正の場合投稿できない' do
        expect do
          post microposts_path, params: { micropost: { picture: '' } }
        end.to change(@user.microposts, :count).by(0)
      end
    end

    context 'ゲストの場合' do
      example '投稿できない' do
        micropost_params = FactoryBot.attributes_for(:examplepost)
        post microposts_path, params: { micropost: micropost_params }
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end

  describe 'delete: DELETE /microposts/:id' do
    context '正常な削除' do
      before do
        @user = FactoryBot.create(:user)
        @other_user = FactoryBot.build(:michael)
        @micropost = FactoryBot.create(:examplepost, user: @user)
        sign_in @user
      end

      example '正常なレスポンスを返す' do
        delete micropost_path(@micropost)
        expect(response.status).to eq 302
      end

      example '自分の投稿を削除する' do
        expect do
          delete micropost_path(@micropost)
        end.to change(@user.microposts, :count).by(-1)
      end
    end

    context '異常な削除' do
      before do
        @user = FactoryBot.create(:user)
        @other_user = FactoryBot.build(:michael)
        @micropost = FactoryBot.create(:examplepost, user: @user)
        sign_in @other_user
      end

      example '他人の投稿は削除できない' do
        expect do
          delete micropost_path(@micropost)
        end.to change(@user.microposts, :count).by(0)
      end
    end
  end
end
