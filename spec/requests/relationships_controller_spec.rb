# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RelationshipsController, type: :request do
  describe 'create: POST /relationships' do
    context 'ログインユーザの場合' do
      before do
        @user = FactoryBot.create(:user)
        @michael = FactoryBot.create(:michael)
        sign_in @user
      end

      example '正常なレスポンスを返す' do
        post relationships_path, params: { followed_id: @michael.id }
        expect(response.status).to eq 302
      end

      example 'userのフォローが増えている' do
        expect do
          post relationships_path, params: { followed_id: @michael.id }
        end.to change(@user.following, :count).by(1)
      end
    end

    context 'ゲストの場合' do
      before do
        @user = FactoryBot.create(:user)
        @michael = FactoryBot.create(:michael)
      end

      example 'ログイン画面に遷移' do
        post relationships_path, params: { followed_id: @michael.id }
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end

  describe 'destroy: DELETE /relationships/:id' do
    context 'ログインユーザの場合' do
      before do
        @user = FactoryBot.create(:user)
        @michael = FactoryBot.create(:michael)
        @follow = FactoryBot.create(:user_to_michael, followed: @michael, follower: @user)
        sign_in @user
      end

      example '正常なレスポンスを返す' do
        delete relationship_path(@follow), params: { followed_id: @michael.id }
        expect(response.status).to eq 302
      end

      example 'userのフォローが減っている' do
        expect do
          delete relationship_path(@follow)
        end.to change(@user.following, :count).by(-1)
      end
    end

    context 'ゲストの場合' do
      before do
        @user = FactoryBot.create(:user)
        @michael = FactoryBot.create(:michael)
      end

      example 'ログイン画面に遷移' do
        post relationships_path, params: { followed_id: @michael.id }
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end
end
