# frozen_string_literal: true

# controllersは非推奨
require 'rails_helper'

RSpec.describe StaticPagesController, type: :request do
  describe 'Homeページ' do
    context 'ログインしている状態'

    example '画面の表示' do
      get root_path
      expect(response).to have_http_status(200)
    end
  end
end
