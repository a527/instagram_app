# frozen_string_literal: true

require 'rails_helper'
RSpec.describe UsersController, type: :request do
  let(:base_title) { 'Fake Instagram' }

  describe 'index: GET /users' do
    context 'ログインユーザの場合' do
      before do
        @user = FactoryBot.create(:user)
        # deviseのメソッド（ヘルパーの使い方がわからなかった）
        sign_in @user
      end

      example '正常なレスポンスを返す' do
        get users_path
        expect(response.status).to eq 200
      end

      example '正常なタイトル' do
        get users_path
        assert_select 'title', "おすすめユーザ | #{base_title}"
      end
    end

    context 'ゲストの場合' do
      example 'フラッシュメッセージが表示されたログイン画面に遷移' do
        get users_path
        expect(flash).to_not be_blank
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end

  describe 'show: GET /user/:id' do
    context 'ログインユーザの場合' do
      before do
        @user = FactoryBot.create(:user)
        sign_in @user
      end

      example '正常なレスポンスを返す' do
        get user_path(@user)
        expect(response.status).to eq 200
      end

      example '正常なタイトル' do
        get user_path(@user)
        assert_select 'title', "#{@user.user_name} | #{base_title}"
      end
    end

    context 'ゲストの場合' do
      before do
        @user = FactoryBot.create(:user)
      end

      it 'ログイン画面にリダイレクトされる' do
        get user_path(@user)
        expect(flash).to_not be_blank
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end

  describe 'edit: GET /user/:id/edit' do
    context 'ログインユーザかつ自身の編集ページの場合' do
      before do
        @user = FactoryBot.create(:user)
        @other_user = FactoryBot.create(:michael)
        sign_in @user
      end

      example '正常なレスポンスを返す' do
        get edit_user_path(@user)
        expect(response.status).to eq 200
      end

      example '正常なタイトル' do
        get edit_user_path(@user)
        assert_select 'title', "ユーザ情報編集 | #{base_title}"
      end
    end

    context 'ログインユーザかつ自身ではない編集ページの場合' do
      before do
        @user = FactoryBot.create(:user)
        @other_user = FactoryBot.create(:michael)
        sign_in @user
      end

      it '302レスポンスを返す' do
        get edit_user_path(@other_user)
        expect(response).to have_http_status '302'
        expect(response).to redirect_to root_url
      end
    end

    context 'ゲストの場合' do
      before do
        @user = FactoryBot.create(:user)
      end
      it 'フラッシュメッセージが表示され、ログイン画面にリダイレクトされる' do
        get edit_user_path(@user)
        expect(flash).to_not be_blank
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end

  describe 'update: PATCH /users/:id' do
    context '正しい編集の場合' do
      before do
        @user = FactoryBot.create(:user)
        sign_in @user
      end

      example '正常なレスポンスを返す' do
        patch user_path(@user), params: { user: { full_name: 'foobar' } }
        expect(response.status).to eq 302
      end

      it 'ユーザー情報を更新できる' do
        patch user_path(@user), params: { user: { full_name: 'foobar' } }
        expect(@user.reload.full_name).to eq 'foobar'
      end

      it '編集後user_pathにリダイレクトされる' do
        patch user_path(@user), params: { user: { full_name: 'foobar' } }
        expect(response).to redirect_to user_path(@user)
      end
    end

    context 'ログインユーザかつ自身ではない編集の場合' do
      before do
        @user = FactoryBot.create(:user)
        @other_user = FactoryBot.create(:michael)
        sign_in @user
      end

      it '302レスポンスを返す' do
        get edit_user_path(@other_user)
        expect(response).to have_http_status '302'
        expect(response).to redirect_to root_url
      end
    end

    context 'ゲストの場合' do
      before do
        @user = FactoryBot.create(:user)
      end

      it 'フラッシュメッセージが表示され、ログインページにリダイレクトされる' do
        user_params = FactoryBot.attributes_for(:user, name: 'yokota')
        patch user_path(@user), params: { user: user_params }
        expect(flash).to_not be_blank
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end

  describe 'destroy: DELETE /user/:id' do
    before do
      @user = FactoryBot.create(:user)
      @other_user = FactoryBot.create(:michael)
      sign_in @user
    end

    context '正しいアカウント削除の場合' do
      example '正常なレスポンスを返す' do
        delete user_path(@user)
        expect(response.status).to eq 302
      end

      example '正常なユーザ削除' do
        expect do
          delete user_path(@user)
        end.to change(User, :count).by(-1)
      end
    end

    context '不正なアカウント削除の場合' do
      example '削除されない' do
        expect do
          delete user_path(@other_user)
        end.to change(User, :count).by(0)
      end
    end
  end

  describe 'following: GET /users/:id/following' do
    context 'ログインユーザの場合' do
      before do
        @user = FactoryBot.create(:user)
        sign_in @user
      end
      example '正常なレスポンスを返す' do
        get following_user_path(@user)
        expect(response.status).to eq 200
      end

      example '正常なタイトル' do
        get following_user_path(@user)
        assert_select 'title', "フォロー | #{base_title}"
      end
    end

    context 'ゲストの場合' do
      example 'ログイン画面にリダイレクトされる' do
        @user = FactoryBot.create(:user)
        get following_user_path(@user)
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end

  describe 'followers: GET /users/:id/followers' do
    context 'ログインユーザの場合' do
      before do
        @user = FactoryBot.create(:user)
        sign_in @user
      end
      example '正常なレスポンスを返す' do
        get followers_user_path(@user)
        expect(response.status).to eq 200
      end

      example '正常なタイトル' do
        get followers_user_path(@user)
        assert_select 'title', "フォロワー | #{base_title}"
      end
    end

    context 'ゲストの場合' do
      example 'ログイン画面にリダイレクトされる' do
        @user = FactoryBot.create(:user)
        get followers_user_path(@user)
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end

  describe 'likes: GET /users/:id/likes' do
    context 'ログインユーザの場合' do
      before do
        @user = FactoryBot.create(:user)
        sign_in @user
      end
      example '正常なレスポンスを返す' do
        get likes_user_path(@user)
        expect(response.status).to eq 200
      end

      example '正常なタイトル' do
        get likes_user_path(@user)
        assert_select 'title', "お気に入り | #{base_title}"
      end
    end

    context 'ゲストの場合' do
      example 'ログイン画面にリダイレクトされる' do
        @user = FactoryBot.create(:user)
        get likes_user_path(@user)
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end
end
