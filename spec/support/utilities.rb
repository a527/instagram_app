# frozen_string_literal: true

module LoginSupport
  def sign_in_as(user)
    sign_in user
  end
end
