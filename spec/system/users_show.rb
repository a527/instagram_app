# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'UsersLogin', type: :feature do
  before do
    @user = FactoryBot.create(:user)
    @other_user = FactoryBot.create(:user2)
  end

  context 'link in own profile' do
    it 'have a link to edit and logout' do
      sign_in_as(@user)
      expect(page).to have_current_path "/users/#{@user.id}"
      expect(page).to have_link '編集'
      expect(page).to have_link 'ログアウト'
    end
  end

  context 'link in other user profile' do
    it 'do not have a link to edit' do
      sign_in_as(@other_user)
      visit user_path(@user)
      expect(page).to have_link '編集', count: 0
      expect(page).to have_link 'ログアウト', count: 0
    end
  end
end
