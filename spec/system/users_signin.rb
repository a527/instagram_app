# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'UsersLogin', type: :feature do
  describe 'ログイン' do
    context 'ログイン成功' do
      it '画面遷移' do
        user = FactoryBot.create(:user)
        visit root_path
        click_link 'ログイン'
        fill_in 'user[email]', with: user.email
        fill_in 'user[password]', with: user.password
        click_button 'ログイン'
        expect(page).to have_current_path "/users/#{user.id}"
      end
      it '画面表示' do
        user = FactoryBot.create(:user)
        sign_in_as(user)
        expect(page).to have_link 'ログイン', count: 0
        expect(page).to have_link 'マイページ'
      end
    end

    context 'ログイン失敗' do
      before do
        user = FactoryBot.create(:user)
        visit root_path
        click_link 'ログイン'
        fill_in 'user[email]', with: ' '
        fill_in 'user[password]', with: ' '
        click_button 'ログイン'
      end

      it '画面遷移' do
        expect(page).to have_current_path '/users/sign_in'
      end
      it '画面表示' do
        expect(page).to have_current_path '/users/sign_in'
        expect(page).to have_link 'マイページ', count: 0
      end
    end
  end

  describe 'ログアウト' do
    context 'ログアウト成功' do
      before do
        user = FactoryBot.create(:user)
        sign_in_as(user)
        visit user_path(user)
        click_link 'ログアウト'
      end
      it '画面遷移' do
        expect(page).to have_current_path '/'
      end
      it '画面表示' do
        expect(page).to have_link 'ログイン'
        expect(page).to have_link 'マイページ', count: 0
      end
    end
  end
end
