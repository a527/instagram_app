# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Users sign_up', type: :request do
  context '新規登録に成功する場合' do
    it 'can add a new user and redirect user page' do
      get new_user_registration_path
      expect(response).to have_http_status(:success)

      expect do
        post user_registration_path, params: { user: { full_name: 'Test User',
                                                       user_name: 'testuser',
                                                       email: 'example@user.com',
                                                       password: 'password',
                                                       password_confirmation: 'password' } }
      end.to change { User.count }.by(1)
      redirect_to @user
      follow_redirect!
      # assert_template 'users/show'
      expect(response).to render_template('users/show')
    end
  end

  context '新規登録に失敗する場合' do
    it 'cannot add a new user and redirect signup page' do
      get new_user_registration_path
      expect do
        post user_registration_path, params: { user: { full_name: '',
                                                       user_name: '',
                                                       email: 'test',
                                                       password: 'pas',
                                                       password_confirmation: 'pas' } }
      end.to change { User.count }.by(0)
      expect(response).to render_template('registrations/new')
    end
  end
end
